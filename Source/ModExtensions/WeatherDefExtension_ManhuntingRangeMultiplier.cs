﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RealisticManhunters
{
    public class WeatherDefExtension_ManhuntingRangeModifier : DefModExtension
    {
        public float multiplier = 1f;
        public float offset = 0f;
    }
}
