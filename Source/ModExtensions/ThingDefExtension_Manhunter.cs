﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RealisticManhunters
{
    public class ThingDefExtension_Manhunter : DefModExtension
    {
        public float smellRange = ManhunterUtility.DefaultManhuntingRange;
        public int roamRange = 90;

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
                yield return error;
            if(smellRange <= 0)
                yield return $"Required field \"{nameof(smellRange)}\" is out of range: {smellRange}: (0 - {float.MaxValue})";
            if(roamRange <= 0)
                yield return $"Required field \"{nameof(roamRange)}\" is out of range: {roamRange}: (0 - {float.MaxValue})";
        }
    }
}
