﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace RealisticManhunters
{
    public class MentalState_RealisticManhunter : MentalState_Manhunter
    {
        const int ticksBetweenTargetChecks = 50;
        const int ticksBetweenLordChecks = 100;

        public override void PreStart()
        {
            base.PreStart();
            Lord lord = ManhunterUtility.FindOrCreateLordFor(pawn);
            if(!lord.ownedPawns.Contains(pawn))
            {
                lord.AddPawn(pawn);
            }
        }

        public override void PostEnd()
        {
            base.PostEnd();
            Lord lord = pawn.GetLord();
            if(lord != null)
            {
                lord.RemovePawn(pawn);
            }
        }

        public override void MentalStateTick()
        {
            base.MentalStateTick();
            if(pawn.Downed)
            {
                return;
            }
            if(pawn.IsHashIntervalTick(ticksBetweenTargetChecks))
            {
                TryFindTarget();
            }
            if(pawn.IsHashIntervalTick(ticksBetweenLordChecks))
            {
                EnsureLord();
            }
        }

        private void TryFindTarget()
        {
            if(pawn.mindState.enemyTarget != null)
            {
                return;
            }

            float range = ManhunterUtility.GetPawnFindingRange(pawn);
            //if(RealisticManhuntersMod.Settings.DevModeEnabled)
            //    Log.Message($"Pawn {pawn} is trying to find a target in range {range}");
            //DebugActions.FlashCellsInRadius(pawn.Map, pawn.Position, range);
            Thing target = TargetUtility.TryFindTarget(pawn, range);
            if(target != null)
            {
                if(RealisticManhuntersMod.Settings.DevModeEnabled)
                {
                    Log.Message($"Detected target {target} at position {target.Position}");
                    pawn.Map.debugDrawer.FlashLine(pawn.Position, target.Position, 100, SimpleColor.Red);
                }
                TargetUtility.SetPackTarget(pawn, target);
            }
        }

        private void EnsureLord()
        {
            if(pawn.GetLord() != null)
            {
                return;
            }
            Lord lord = ManhunterUtility.FindOrCreateLordFor(pawn);
            if(!lord.ownedPawns.Contains(pawn))
            {
                lord.AddPawn(pawn);
            }
        }

        public override bool ForceHostileTo(Thing t)
        {
            if(!TargetUtility.IsValidTargetFor(pawn, t))
            {
                return false;
            }
            return true;
        } 
    }
}
