﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RealisticManhunters
{
    public static class PositionUtility
    {
        public static int DefaultRoamRadius => RealisticManhuntersMod.Settings.FallbackRoamRadius;

        public static IntVec3 FindRandomRoamPosition(Map map)
        {
            int x = GetRandomRoamCellOnLine(map.Size.x);
            int z = GetRandomRoamCellOnLine(map.Size.z);
            return new IntVec3(x, 0, z);
        }

        public static IntVec3 FindRandomRoamPositionFor(Pawn pawn)
        {
            ThingDefExtension_Manhunter extension = pawn.def.GetModExtension<ThingDefExtension_Manhunter>();
            int radius = DefaultRoamRadius;
            if(extension != null)
            {
                radius = extension.roamRange;
            }
            return CellFinder.RandomClosewalkCellNear(pawn.Position, pawn.Map, radius);
        }

        private static int GetRandomRoamCellOnLine(int length)
        {
            const int edgePadding = 15;

            return new IntRange(edgePadding, length - edgePadding).RandomInRange;
        }

        public static IntVec3 DetermineCenter(IEnumerable<IntVec3> positions)
        {
            return new IntVec3((int)Math.Round(positions.Average(p => p.x)), 0, (int)Math.Round(positions.Average(p => p.z)));
        }
    }
}
