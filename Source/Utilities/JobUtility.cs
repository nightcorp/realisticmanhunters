﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RealisticManhunters
{
    public static class JobUtility
    {
        public const int additionalRangeToKeepTarget = 6;

        private static FieldInfo targetAcquireRadiusInfo = AccessTools.Field(typeof(JobGiver_AIFightEnemy), "targetAcquireRadius");
        private static FieldInfo targetKeepRadiusInfo = AccessTools.Field(typeof(JobGiver_AIFightEnemy), "targetKeepRadius");
        private static float previousTargetAcquireRadius;
        private static float previousTargetKeepRadius;
        public static void OverrideRanges(this JobGiver_AIFightEnemy jobGiver, Pawn pawn)
        {
            previousTargetAcquireRadius = (float)targetAcquireRadiusInfo.GetValue(jobGiver);
            previousTargetKeepRadius = (float)targetKeepRadiusInfo.GetValue(jobGiver);
            float newRange = ManhunterUtility.GetPawnFindingRange(pawn);
            targetAcquireRadiusInfo.SetValue(jobGiver, newRange);
            targetKeepRadiusInfo.SetValue(jobGiver, newRange + additionalRangeToKeepTarget);
        }

        public static void ResetRanges(this JobGiver_AIFightEnemy jobGiver)
        {
            targetAcquireRadiusInfo.SetValue(jobGiver, previousTargetAcquireRadius);
            targetKeepRadiusInfo.SetValue(jobGiver, previousTargetKeepRadius);
        }
    }
}
