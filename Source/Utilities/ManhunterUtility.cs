﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI.Group;

namespace RealisticManhunters
{
    public static class ManhunterUtility
    {
        public static float DefaultManhuntingRange => RealisticManhuntersMod.Settings.DefaultManhuntingRange;

#if v1_4 || v1_3
        public static MethodInfo CanArriveManhunterMethod = AccessTools.Method(typeof(ManhunterPackIncidentUtility), "CanArriveManhunter");
#else
        public static MethodInfo CanArriveManhunterMethod = AccessTools.Method(typeof(AggressiveAnimalIncidentUtility), "CanArriveManhunter");
#endif
        /// <summary>
        /// <see cref="ManhunterPackIncidentUtility"/>.CanArriveManhunter(PawnKindDef kind) is sadly private. Reflection workaround
        /// </summary>
        public static bool CanArriveManhunter(PawnKindDef kind)
        {
            return (bool)CanArriveManhunterMethod.Invoke(null, new Object[] { kind });
        }

        private static List<ThingDef> _manhunterRaces;
        public static List<ThingDef> ManhunterRaces
        {
            get
            {
                if(_manhunterRaces == null)
                {
                    CacheManhunters();
                }
                return _manhunterRaces;
            }
        }
        private static List<PawnKindDef> _manhunterPawnKinds;
        public static List<PawnKindDef> ManhunterPawnKinds
        {
            get
            {
                if(_manhunterPawnKinds == null)
                {
                    CacheManhunters();
                }
                return _manhunterPawnKinds;
            }
        }
        private static void CacheManhunters()
        {
            _manhunterPawnKinds = DefDatabase<PawnKindDef>.AllDefsListForReading
                .Where(kind => ManhunterUtility.CanArriveManhunter(kind))
                .ToList();

            _manhunterRaces = _manhunterPawnKinds
                .Select(kind => kind.race)
                .ToList();
        }



        public static float GetPawnFindingRange(Pawn pawn)
        {
            ThingDefExtension_Manhunter extension = pawn.def.GetModExtension<ThingDefExtension_Manhunter>();
            float range;
            if(extension == null)
            {
                range = ManhunterUtility.DefaultManhuntingRange;
            }
            else
            {
                range = extension.smellRange;
            }

            ModifyPawnFindingRangeWithWeather(pawn.Map, ref range);

            return range;
        }

        public static void ModifyPawnFindingRangeWithWeather(Map map, ref float range)
        {
            WeatherDef weather = map.weatherManager.curWeather;
            WeatherDefExtension_ManhuntingRangeModifier extension = weather?.GetModExtension<WeatherDefExtension_ManhuntingRangeModifier>();
            if(extension == null)
            {
                return;
            }

            range *= extension.multiplier;
            range += extension.offset;
        }

        public static Lord FindOrCreateLordFor(Pawn pawn)
        {
            List<Lord> lords = pawn.Map.lordManager.lords;
            Lord matchingMembersLord = lords.Find(lord => lord.IsLordSuitableFor(pawn));
            if(matchingMembersLord != null)
            {
                if(RealisticManhuntersMod.Settings.DevModeEnabled)
                    Log.Message($"Found existing lord for pawn {pawn} of race {pawn.def.defName}");
                return matchingMembersLord;
            }
            if(RealisticManhuntersMod.Settings.DevModeEnabled)
                Log.Message($"Created lord for pawn {pawn} of race {pawn.def.defName}");
            return LordMaker.MakeNewLord(null, new LordJob_Manhunter(), pawn.Map, new Pawn[] { pawn });
        }

        public static bool IsLordSuitableFor(this Lord lord, Pawn pawn)
        {
            Pawn firstPawn = lord.ownedPawns.FirstOrDefault();
            if(firstPawn == null)
            {
                return false;
            }
            if(pawn.RaceProps.Insect && firstPawn.RaceProps.Insect)
            {
                return true;
            }
            if(pawn.def == firstPawn.def)
            {
                return true;
            }
            return false;
        }
    }
}
