﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using Verse.AI.Group;

namespace RealisticManhunters
{
    public static class SignalUtility
    {
        public static string ToString(Signal signal)
        {
            return $"tag={signal.tag}; args={signal.args.Args.ToStringSafeEnumerable()}";
        }

        public static string ToString(TriggerSignal signal)
        {
            return $"type={signal.type}; " +
                $"memo={signal.memo}; " +
                $"signal={ToString(signal.signal)}; " +
                $"thing={signal.thing.ThingID}; " +
                $"condition={signal.condition}; " +
                $"faction={signal.faction}; " +
                $"clamorType={signal.clamorType}; " +
                $"previousRelationKind={signal.previousRelationKind}";
        }
    }
}
