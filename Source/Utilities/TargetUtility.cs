﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace RealisticManhunters
{
    public static class TargetUtility
    {

        public static Thing TryFindTarget(Pawn pawn, float range)
        {
            TargetScanFlags pawnTargetFlags = TargetScanFlags.NeedAutoTargetable | TargetScanFlags.NeedReachable;
            Pawn targetPawn = (Pawn)AttackTargetFinder.BestAttackTarget(pawn, pawnTargetFlags, (Thing potentialTarget) => potentialTarget is Pawn && IsValidTargetFor(pawn, potentialTarget), 0f, range, canBashFences: true);
            if(targetPawn != null)
            {
                return targetPawn;
            }
            TargetScanFlags turretTargetFlags = TargetScanFlags.NeedLOSToNonPawns | TargetScanFlags.NeedReachable | TargetScanFlags.NeedActiveThreat | TargetScanFlags.NeedAutoTargetable;
            Thing targetTurret = (Thing)AttackTargetFinder.BestAttackTarget(pawn, turretTargetFlags, validator: (t => t is Building), maxDist: 70f);
            if(targetTurret != null)
            {
                return targetTurret;
            }
            return null;
        }

        public static bool IsValidTarget(Thing potentialTarget)
        {
            if(potentialTarget is Building_Turret)
            {
                return true;
            }
            if(!(potentialTarget is Pawn))
            {
                return false;
            }
            bool isIntelligenceValid = RealisticManhuntersMod.Settings.ManhuntersAttackAnythingThatMoves
                || potentialTarget.def.race.intelligence >= Intelligence.ToolUser;
            if(!isIntelligenceValid)
            {
                return false;
            }
            return true;
        }

        public static bool IsValidTargetFor(Pawn searcher, Thing potentialTarget)
        {
            if(!IsValidTarget(potentialTarget))
            {
                return false;
            }
            // the LordJob should run this validation, but... it doesn't? So we manually make sure that the target is not in the same lord as the searcher
            if(potentialTarget is Pawn targetPawn)
            {
                if (searcher.def == targetPawn.def)
                {
                    // if the target is of the same race, only attack them if the current pawn is starving
                    Need_Food searcherFood = searcher.needs?.food;
                    if (searcherFood == null || !searcherFood.Starving)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static Pawn TryFindDownedPawn(Pawn pawn, float range)
        {
            TargetScanFlags pawnTargetFlags = TargetScanFlags.NeedReachable;
            Predicate<Thing> validator = (Thing potentialTarget) =>
            {
                return potentialTarget is Pawn potentialPawn
                    && potentialPawn.Downed;
            };
            Pawn targetPawn = (Pawn)AttackTargetFinder.BestAttackTarget(pawn, pawnTargetFlags, validator, 0f, range);
            return targetPawn;
        }

        public static void SetPackTarget(Pawn pawn, Thing target)
        {
            Lord lord = pawn.GetLord();
            if(lord == null)
            {
                Log.Warning($"Pawn {pawn} detected target {target} for manhunting, but does not have a Lord to pass to");
                return;
            }
            Signal signal = new Signal(
                Trigger_Signal_TargetAcquired.SignalTag,
                pawn.Named(Trigger_Signal_TargetAcquired.FindingPawnTag),
                target.Named(Trigger_Signal_TargetAcquired.AcquiredTargetTag)
            );
            lord.Notify_SignalReceived(signal);
        }
    }
}
