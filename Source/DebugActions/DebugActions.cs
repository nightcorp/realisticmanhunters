﻿#if !v1_4 && !v1_3
using LudeonTK;
#endif
using RimWorld;
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;
using UnityEngine;
using Verse.AI.Group;
using Verse.AI;

namespace RealisticManhunters
{
    public static class DebugActions
    {
        [DebugAction("RealisticManhunters", "Show manhunt range", actionType = DebugActionType.ToolMapForPawns, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void ShowManhuntingRange(Pawn p)
        {
            float range = ManhunterUtility.GetPawnFindingRange(p);
            FlashCellsInRadius(p.Map, p.Position, range);
        }

        public static void FlashCellsInRadius(Map map, IntVec3 position, float radius, Func<IntVec3, string> textForCell = null, Func<IntVec3, float> colorForCell = null)
        {
            bool isTooLargeToVisualize = radius > GenRadial.MaxRadialPatternRadius;
            string floatText = null;
            if(isTooLargeToVisualize)
            {
                floatText = ($"Reached max radial pattern for visual flashing, the shown radius of {GenRadial.MaxRadialPatternRadius} is smaller than the actual radius of {radius}");
                Log.Warning(floatText);
            }
            else
            {
                floatText = $"Range / Radius: {radius}";
            }
            radius = Math.Min(radius, GenRadial.MaxRadialPatternRadius - 0.4f);
            IEnumerable<IntVec3> cells = GenRadial.RadialCellsAround(position, radius, true);

            Find.WindowStack.Add(new FloatMenu(new List<FloatMenuOption>() { new FloatMenuOption(floatText, () => { }) }));
            foreach(IntVec3 cell in cells)
            {
                float color = colorForCell != null ? colorForCell(cell) : 0.2f;

                string text = textForCell != null ? textForCell(cell) : "";
                map.debugDrawer.FlashCell(cell, color, text);
            }
        }

        [DebugAction("RealisticManhunters", "Spawn all manhunters once", actionType = DebugActionType.ToolMap, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void SpawnAllManhunterAnimals()
        {
            Map map = Find.CurrentMap;
            IntVec3 position = UI.MouseCell();
            int spawnCount = ManhunterUtility.ManhunterPawnKinds.Count;
            IEnumerable<IntVec3> cells = GenRadial.RadialCellsAround(position, GenRadial.RadiusOfNumCells(spawnCount), true);
            for(int i = 0; i < spawnCount; i++)
            {
                PawnKindDef kindDef = ManhunterUtility.ManhunterPawnKinds[i];
                Pawn pawn = PawnGenerator.GeneratePawn(new PawnGenerationRequest(kindDef));
                IntVec3 cell = cells.ElementAt(i);
                GenSpawn.Spawn(pawn, cell, map);
            }
        }

        [DebugAction("RealisticManhunters", "Flash random roam cell", actionType = DebugActionType.ToolMap, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void FlashRandomRoamCell()
        {
            Map map = Find.CurrentMap;
            IntVec3 cell = PositionUtility.FindRandomRoamPosition(map);
            map.debugDrawer.FlashCell(cell, 0.9f);
        }

        [DebugAction("RealisticManhunters", "Force roam dest", actionType = DebugActionType.ToolMapForPawns, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void ForceRoamDest(Pawn p)
        {
            if(p.GetLord()?.CurLordToil is LordToil_Roam roamToil)
            {
                TargetingParameters param = new TargetingParameters()
                {
                    canTargetLocations = true,
                    canTargetPawns = false,
                    canTargetBuildings = false,
                    canTargetAnimals = false,
                    canTargetHumans = false,
                    canTargetMechs = false,
                    mapObjectTargetsMustBeAutoAttackable = false,
#if !v1_3
                    canTargetBloodfeeders = false,
#endif
                };
                Find.Targeter.BeginTargeting(param, (LocalTargetInfo target) =>
                {
                    roamToil.ForceRoamPosition(target.Cell);
                });
                roamToil.ForceRoamPosition(UI.MouseCell());
            }
        }

        [DebugAction("RealisticManhunters", "Check hostile", actionType = DebugActionType.ToolMap, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void CheckHostile()
        {
            Thing firstThing = UI.MouseCell().GetFirstThing<Thing>(Find.CurrentMap);
            TargetingParameters param = new TargetingParameters()
            {
                canTargetPawns = true,
                canTargetBuildings = true,
                canTargetAnimals = true,
            };
            Find.Targeter.BeginTargeting(param, (LocalTargetInfo target) =>
            {
                Thing secondThing = target.Thing;
                Log.Message($"Thing {firstThing} is hostile to {secondThing} ? {GenHostility.HostileTo(firstThing, secondThing)}");
            });
        }

        [DebugAction("RealisticManhunters", "Get pred/prey score", actionType = DebugActionType.ToolMapForPawns, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void GetPreyScore(Pawn p)
        {
            TargetingParameters param = new TargetingParameters()
            {
                canTargetLocations = false,
                canTargetPawns = true,
                canTargetBuildings = false,
                canTargetAnimals = true,
                canTargetHumans = true,
                canTargetMechs = true,
                mapObjectTargetsMustBeAutoAttackable = true,
#if !v1_3
                canTargetBloodfeeders = true,
#endif
            };
            BeginTargeting();

            void BeginTargeting()
            {
                Find.Targeter.BeginTargeting(param, (LocalTargetInfo target) =>
                {
                    if(target.HasThing && target.Thing is Pawn targetPawn)
                    {
                        Log.Message($"Score: {FoodUtility.GetPreyScoreFor(p, targetPawn)}");
                    }
                    BeginTargeting();
                });
            }
        }


        [DebugAction("RealisticManhunters", "Empty food", actionType = DebugActionType.ToolMapForPawns, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void ClearFoodFor(Pawn p)
        {
            Need_Food foodNeed = p.needs?.food;
            if(foodNeed == null)
            {
                return;
            }
            foodNeed.CurLevel = 0;
        }


        [DebugAction("RealisticManhunters", "Get center of pack", actionType = DebugActionType.ToolMapForPawns, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void GetCenterOfPack(Pawn p)
        {
            Lord lord = p.GetLord();
            if(lord == null)
            {
                return;
            }
            IntVec3 center = PositionUtility.DetermineCenter(lord.ownedPawns.Select(m => m.Position));
            foreach(Pawn member in lord.ownedPawns)
            {
                member.Map.debugDrawer.FlashLine(member.Position, center, 100, SimpleColor.Yellow);
            }
        }
    }
}
