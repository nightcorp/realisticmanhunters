﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace RealisticManhunters
{
#if v1_4 || v1_3
    [HarmonyPatch(typeof(IncidentWorker_ManhunterPack), "TryExecuteWorker")]
#else
    [HarmonyPatch(typeof(IncidentWorker_AggressiveAnimals), "TryExecuteWorker")]
#endif
    public static class Patch_IncidentWorker_ManhunterPack
    {
        /// <summary>
        /// Inject code at the end of the original manhunter incident worker
        /// 
        /// - Create Lord for spawned manhunters
        /// - Increase map-leaving timer
        /// </summary>
        /// <param name="instructions"></param>
        /// <returns></returns>
        [HarmonyTranspiler]
        public static IEnumerable<CodeInstruction> ChangeManhuntingBehaviour(IEnumerable<CodeInstruction> instructions)
        {
            CodeInstruction anchor = instructions.Last(i => i.opcode == OpCodes.Ldc_I4_1);  // the last instruction is `return TRUE`, we catch the last loading of `TRUE` with this
            foreach(CodeInstruction instruction in instructions)
            {
                if(instruction == anchor)
                {
                    yield return new CodeInstruction(OpCodes.Ldloc_3);  // load list of pawns (manhunter animals)
                    yield return new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(Patch_IncidentWorker_ManhunterPack), nameof(ModifyPawns)));    // call helper
                }
                yield return instruction;
            }
        }

        static float ExitMapTimeMultiplier => RealisticManhuntersMod.Settings.ExitMapTimeMultiplier;
        private static void ModifyPawns(List<Pawn> pawns)
        {
            foreach(Pawn pawn in pawns)
            {
                pawn.mindState.exitMapAfterTick = Mathf.RoundToInt(GenTicks.TicksGame + (pawn.mindState.exitMapAfterTick - GenTicks.TicksGame) * ExitMapTimeMultiplier);  // increase the time it takes for manhunters to leave the map
            }
        }
    }
}
