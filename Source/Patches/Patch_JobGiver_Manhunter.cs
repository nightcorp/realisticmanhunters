﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RealisticManhunters.Patches
{
    [HarmonyPatch(typeof(JobGiver_Manhunter), "FindPawnTarget")]
    public static class Patch_JobGiver_Manhunter
    {
        /// <summary>
        /// Hook into the target findings range value loading and replace it with access to the <see cref="ThingDefExtension_Manhunter"/> of the pawn.
        /// This also effectively reduces the global manhunter range finding to the global default provided in <see cref="ManhunterUtility.DefaultManhuntingRange"/>
        /// </summary>
        /// <param name="instructions"></param>
        /// <returns></returns>
        [HarmonyTranspiler]
        public static IEnumerable<CodeInstruction> InjectCustomPawnFindingRange(IEnumerable<CodeInstruction> instructions)
        {
            foreach(CodeInstruction instruction in instructions)
            {
                if(instruction.operand is float number && number == 9999f)
                {
                    yield return new CodeInstruction(OpCodes.Ldarg_1);
                    yield return new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(ManhunterUtility), nameof(ManhunterUtility.GetPawnFindingRange)));
                }
                else
                {
                    yield return instruction;
                }
            }
        }
    }
}
