﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RealisticManhunters
{
    [HarmonyPatch(typeof(Need_Food), nameof(Need_Food.FoodFallPerTickAssumingCategory))]
    public static class Patch_Need_Food
    {
        /// <summary>
        /// Pawn is protected, need to use reflection to access it
        /// </summary>
        static FieldInfo pawnFieldInfo = AccessTools.Field(typeof(Need), "pawn");
        static float ScariaFoodFallRateMultiplier => RealisticManhuntersMod.Settings.ScariaFoodFallRateMultiplier;
        [HarmonyPostfix]
        public static void ReduceFoodFallRateForScaria(Need_Food __instance, ref float __result)
        {
            Pawn pawn = pawnFieldInfo.GetValue(__instance) as Pawn;
            if(pawn?.health?.hediffSet?.HasHediff(HediffDefOf.Scaria) == true)
            {
                __result *= ScariaFoodFallRateMultiplier;
            }
        }
    }
}
