﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using Verse.AI.Group;

namespace RealisticManhunters
{
    [HarmonyPatch(typeof(FoodUtility))]
    public static class Patch_FoodUtility
    {
        [HarmonyPatch("FoodOptimality")]
        [HarmonyPostfix]
        public static void DecreaseFoodOptimalityOfPackMembers(ref float __result, Pawn eater, Thing foodSource)
        {
            if(!(foodSource is Pawn otherPawn))
            {
                return;
            }
            Lord lord = eater.GetLord();
            if(lord != null && otherPawn.GetLord() == lord)
            {
                if(RealisticManhuntersMod.Settings.DevModeEnabled)
                    Log.Message($"Pawn {eater} is considering to eat another pack member {otherPawn}");
                __result = Mathf.Min(__result, -1);
            }
        }

        [HarmonyPatch("GetPreyScoreFor")]
        [HarmonyPostfix]
        public static void IncreasePreyScoreForPack(ref float __result, Pawn predator, Pawn prey)
        {
            Lord lord = predator.GetLord();
            if(lord == null)
            {
                return;
            }
            if(RealisticManhuntersMod.Settings.DevModeEnabled)
                Log.Message($"Initiated pack-hunting score calculations, predator: {predator}, prey: {prey}");
            /// This is mostly the base logic, but the combat power of the predator is increased with the size of the pack
            float packPowerMultiplier = packSizeToCombatMultiplier.Evaluate(lord.ownedPawns.Count);
            float relativeCombatPower = prey.kindDef.combatPower / (predator.kindDef.combatPower * packPowerMultiplier);
            // the rest is straight up copied from base-game
            float preyHealthScale = prey.health.summaryHealth.SummaryHealthPercent;
            float bodySizeFactor = prey.ageTracker.CurLifeStage.bodySizeFactor;
            float distance = (predator.Position - prey.Position).LengthHorizontal;
            if(prey.Downed)
            {
                preyHealthScale = Mathf.Min(preyHealthScale, 0.2f);
            }
            float newScore = -distance - 56f * preyHealthScale * preyHealthScale * relativeCombatPower * bodySizeFactor;
            if(prey.RaceProps.Humanlike)
            {
                newScore -= 35f;
            }
            else if(IsPreyProtectedFromPredatorByFence(predator, prey))
            {
                newScore -= 17f;
            }
            if(RealisticManhuntersMod.Settings.DevModeEnabled)
                Log.Message($"Pack hunting increased prey score, would have been: {__result}, but is now {newScore} due to combat multiplier: {packPowerMultiplier}");
            __result = newScore;
        }
        private static SimpleCurve packSizeToCombatMultiplier = new SimpleCurve()
        {
            new CurvePoint(1, 1),
            new CurvePoint(4, 3),
            new CurvePoint(8, 5),
        };

        private static MethodInfo isPreyProtectedFromPredatorByFenceInfo = AccessTools.Method(typeof(FoodUtility), "IsPreyProtectedFromPredatorByFence");
        private static bool IsPreyProtectedFromPredatorByFence(Pawn predator, Pawn prey)
        {
            return (bool)isPreyProtectedFromPredatorByFenceInfo.Invoke(null, new object[] { predator, prey });
        }
    }
}
