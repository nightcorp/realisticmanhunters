﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse.AI;

namespace RealisticManhunters
{
    [DefOf]
    public static class ReMa_DutyDefOf
    {
        static ReMa_DutyDefOf()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(ReMa_DutyDefOf));
        }

        public static DutyDef ReMa_Roam;
        public static DutyDef ReMa_Escort;
        public static DutyDef ReMa_GetFood;
        public static DutyDef ReMa_HuntTarget;
        public static DutyDef ReMa_KillDownedPawns;

    }
}
