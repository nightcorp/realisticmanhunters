﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI.Group;

namespace RealisticManhunters
{
    public class Trigger_MembersHungry : Trigger
    {
        int ticksBetweenChecks;
        Predicate<HungerCategory> predicate;
        float requiredRatio;
        bool inverted;

        /// <param name="requiredRatio">If left at -1, this will pull the value from the settings</param>
        public Trigger_MembersHungry(Predicate<HungerCategory> predicate, int ticksBetweenChecks = 1, float requiredRatio = -1, bool inverted = false)
        {
            this.ticksBetweenChecks = ticksBetweenChecks;
            this.predicate = predicate;
            if(requiredRatio == -1)
            {
                requiredRatio = RealisticManhuntersMod.Settings.HungryMembersRatioToEngageFeedingToil;
            }
            this.requiredRatio = requiredRatio;
            this.inverted = inverted;
        }

        public override bool ActivateOn(Lord lord, TriggerSignal signal)
        {
            if(GenTicks.TicksGame % ticksBetweenChecks != 0)
            {
                return false;
            }
            int hungryPawns = lord.ownedPawns.Count(pawn => predicate(pawn.needs.food.CurCategory));
            float ratio = (float)hungryPawns / (float)lord.ownedPawns.Count;
            if(RealisticManhuntersMod.Settings.DevModeEnabled)
                Log.Message($"Members matching hunger predicate: {hungryPawns}, ratio: {ratio} / {requiredRatio}");
            bool passed = ratio >= requiredRatio;
            if(inverted)
            {
                return !passed;
            }
            return passed;
        }
    }
}
