﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace RealisticManhunters
{
    public class Trigger_Manhunter_PawnHarmed : Trigger_PawnHarmed
    {
#if !v1_5
        public Trigger_Manhunter_PawnHarmed(float chance = 1f, bool requireInstigatorWithFaction = false, Faction requireInstigatorWithSpecificFaction = null) : base(chance, requireInstigatorWithFaction, requireInstigatorWithSpecificFaction) { }
#else
        public Trigger_Manhunter_PawnHarmed(float chance = 1f, bool requireInstigatorWithFaction = false, Faction requireInstigatorWithSpecificFaction = null, DutyDef skipDuty = null, int? minTicks = null) : base(chance, requireInstigatorWithFaction, requireInstigatorWithSpecificFaction, skipDuty, minTicks) { }
#endif

        public override bool ActivateOn(Lord lord, TriggerSignal signal)
        {
            if(!base.ActivateOn(lord, signal))
            {
                return false;
            }
            if(!(lord.LordJob is LordJob_Manhunter job))
            {
                Log.Warning($"Received signal, but current LordJob is not {nameof(LordJob_Manhunter)}, instead it's {lord.LordJob.GetType()}");
                return false;
            }
            if(RealisticManhuntersMod.Settings.DevModeEnabled)
                Log.Message($"Member was damaged: {SignalUtility.ToString(signal)}");
            Thing acquiredTarget = signal.dinfo.Instigator;
            job.currentAttackTarget = acquiredTarget;
            job.lastAttackedPackMember = signal.thing as Pawn;
            return true;
        }
    }
}
