﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI.Group;

namespace RealisticManhunters
{
    public class Trigger_Signal_TargetAcquired : Trigger_Signal
    {
        public const string SignalTag = "MemberAcquiredTarget";
        public const string FindingPawnTag = "Finder";
        public const string AcquiredTargetTag = "AcquiredTarget";

        public Trigger_Signal_TargetAcquired(string signal) : base(signal) { }

        public override bool ActivateOn(Lord lord, TriggerSignal signal)
        {
            bool activateOn = base.ActivateOn(lord, signal);
            if(!activateOn)
            {
                return false;
            }
            if(!(lord.LordJob is LordJob_Manhunter job))
            {
                Log.Warning($"Received signal, but current LordJob is not {nameof(LordJob_Manhunter)}, instead it's {lord.LordJob.GetType()}");
                return false;
            }
            if(RealisticManhuntersMod.Settings.DevModeEnabled)
                Log.Message($"Received signal: {SignalUtility.ToString(signal.signal)}");
            Thing acquiredTarget = signal.signal.args.GetArg<Thing>(AcquiredTargetTag);
            job.currentAttackTarget = acquiredTarget;
            return true;
        }
    }
}
