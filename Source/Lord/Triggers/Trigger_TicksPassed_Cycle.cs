﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI.Group;

namespace RealisticManhunters
{
    public class Trigger_TicksPassed_Cycle : Trigger_TicksPassed
    {
        public Trigger_TicksPassed_Cycle(int tickLimit) : base(tickLimit) { }

        public override bool ActivateOn(Lord lord, TriggerSignal signal)
        {
            if(!base.ActivateOn(lord, signal))
            {
                return false;
            }
            Data.ticksPassed = 0;
            return true;
        }
    }

    public class Trigger_TicksPassed_Cycle_AfterConditionMet : Trigger_TicksPassed
    {
        private bool conditionMetOnce;
        private Func<bool> condition;
        private int checkEveryTicks;

        public Trigger_TicksPassed_Cycle_AfterConditionMet(int tickLimit, Func<bool> condition, int checkEveryTicks = 1) : base(tickLimit)
        {
            this.condition = condition;
            this.checkEveryTicks = checkEveryTicks;
        }

        /// <remarks>
        /// If condition not matched yet, check if in interval, if in interval and condition matches, persist the match and reset timer
        /// Afterwards defer to base tickspassed, then reset the condition matched flag
        /// </remarks>
        public override bool ActivateOn(Lord lord, TriggerSignal signal)
        {
            if(!conditionMetOnce)
            {
                if(GenTicks.TicksGame % checkEveryTicks != 0)
                {
                    return false;
                }
                if(condition())
                {
                    conditionMetOnce = true;
                    Data.ticksPassed = 0;
                }
                else
                {
                    return false;
                }
            }

            if(!base.ActivateOn(lord, signal))
            {
                return false;
            }

            conditionMetOnce = false;
            return true;
        }
    }
}
