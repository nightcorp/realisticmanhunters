﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace RealisticManhunters
{
    public class LordJob_Manhunter : LordJob
    {
        public Thing currentAttackTarget;
        public IntVec3 currentForcedRoamPosition = IntVec3.Invalid;
        public Pawn lastAttackedPackMember;

        public override bool AddFleeToil => false;
        public static int TicksPerRoam => RealisticManhuntersMod.Settings.TicksPerRoam;
        static float RatioOfArrivedPawnsToRoam => RealisticManhuntersMod.Settings.RatioOfArrivedPawnsToNextRoam;

        public override bool ValidateAttackTarget(Pawn searcher, Thing target)
        {
            if(!base.ValidateAttackTarget(searcher, target))
            {
                return false;
            }
            if(target is Building targetBuilding && lord.ownedBuildings.Contains(targetBuilding))
            {
                return false;
            }
            else if(target is Pawn targetPawn && lord.ownedPawns.Contains(targetPawn))
            {
                return false;
            }
            return true;
        }

        public override StateGraph CreateGraph()
        {
            StateGraph stateGraph = new StateGraph();

            // roam
            LordToil roamToil = new LordToil_Roam();
            stateGraph.AddToil(roamToil);
            stateGraph.StartingToil = roamToil;

            Transition t_roamCycle = new Transition(roamToil, roamToil, true);
            t_roamCycle.AddTrigger(new Trigger_TicksPassed_Cycle_AfterConditionMet(TicksPerRoam, MostMembersArrived, 100));
            t_roamCycle.AddPreAction(new TransitionAction_LogTransition(roamToil));
            stateGraph.AddTransition(t_roamCycle);

            // food
            Predicate<HungerCategory> hungryPredicate = cat => cat >= HungerCategory.Hungry;
            Predicate<HungerCategory> fedPredicate = cat => cat == HungerCategory.Fed;
            LordToil foodToil = new LordToil_GetFood(hungryPredicate);
            stateGraph.AddToil(foodToil);

            Transition t_roamToFood = new Transition(roamToil, foodToil);
            t_roamToFood.AddTrigger(new Trigger_MembersHungry(hungryPredicate, 100));
            t_roamToFood.AddPreAction(new TransitionAction_EndAllJobs());
            t_roamToFood.AddPreAction(new TransitionAction_LogTransition(roamToil));
            stateGraph.AddTransition(t_roamToFood);

            Transition t_foodToRoam = new Transition(foodToil, roamToil);
            t_foodToRoam.AddTrigger(new Trigger_MembersHungry(fedPredicate, 100, 1));
            //t_foodToRoam.AddPreAction(new TransitionAction_EndAllJobs());
            t_foodToRoam.AddPreAction(new TransitionAction_LogTransition(foodToil));
            stateGraph.AddTransition(t_foodToRoam);

            // attack
            LordToil attackToil = new LordToil_AttackAsPack();
            stateGraph.AddToil(attackToil);

            Transition t_roamToAttack = new Transition(roamToil, attackToil);
            t_roamToAttack.AddTrigger(new Trigger_Signal_TargetAcquired(Trigger_Signal_TargetAcquired.SignalTag));
            //t_roamToAttack.AddTrigger(new Trigger_TickCondition(MemberDamaged, 50));
            t_roamToAttack.AddTrigger(new Trigger_Manhunter_PawnHarmed());
            t_roamToAttack.AddPreAction(new TransitionAction_EndAllJobs());
            t_roamToAttack.AddPreAction(new TransitionAction_LogTransition(roamToil));
            stateGraph.AddTransition(t_roamToAttack);

            Transition t_foodToAttack = new Transition(foodToil, attackToil);
            t_foodToAttack.AddTrigger(new Trigger_Signal_TargetAcquired(Trigger_Signal_TargetAcquired.SignalTag));
            //t_roamToAttack.AddTrigger(new Trigger_TickCondition(MemberDamaged, 50));
            t_roamToAttack.AddTrigger(new Trigger_Manhunter_PawnHarmed());
            t_foodToAttack.AddPreAction(new TransitionAction_EndAllJobs());
            t_foodToAttack.AddPreAction(new TransitionAction_LogTransition(foodToil));
            stateGraph.AddTransition(t_foodToAttack);

            // kill downed pawns
            // if enabled, transition to LordToil for killing, otherwise always transition back to roaming
            if(RealisticManhuntersMod.Settings.ManhuntersKillDownedPawns)
            {
                LordToil killToil = new LordToil_KillDownedPawns();
                stateGraph.AddToil(killToil);

                Transition t_attackToKill = new Transition(attackToil, killToil);
                t_attackToKill.AddTrigger(new Trigger_TickCondition(NoHostileAvailable, 100));
                t_attackToKill.AddPreAction(new TransitionAction_LogTransition(attackToil));
                stateGraph.AddTransition(t_attackToKill);

                Transition t_killToAttack = new Transition(killToil, attackToil);
                t_killToAttack.AddTrigger(new Trigger_Signal_TargetAcquired(Trigger_Signal_TargetAcquired.SignalTag));
                //t_roamToAttack.AddTrigger(new Trigger_TickCondition(MemberDamaged, 50));
                t_roamToAttack.AddTrigger(new Trigger_Manhunter_PawnHarmed());
                t_killToAttack.AddPreAction(new TransitionAction_EndAllJobs());
                t_killToAttack.AddPreAction(new TransitionAction_LogTransition(killToil));
                stateGraph.AddTransition(t_killToAttack);

                Transition t_killToRoam = new Transition(killToil, roamToil);
                t_killToRoam.AddTrigger(new Trigger_TickCondition(NoDownedPawnsAvailable, 100));
                //t_killToRoam.AddPreAction(new TransitionAction_EndAllJobs());
                t_killToRoam.AddPreAction(new TransitionAction_LogTransition(killToil));
                t_killToRoam.AddPreAction(new TransitionAction_SetForcedRoamPosition());
                stateGraph.AddTransition(t_killToRoam);
            }
            else
            {
                Transition t_attackToRoam = new Transition(attackToil, roamToil);
                t_attackToRoam.AddTrigger(new Trigger_TickCondition(NoHostileAvailable, 100));
                //t_attackToRoam.AddPreAction(new TransitionAction_EndAllJobs());
                t_attackToRoam.AddPreAction(new TransitionAction_SetForcedRoamPosition());
                t_attackToRoam.AddPreAction(new TransitionAction_LogTransition(attackToil));
                stateGraph.AddTransition(t_attackToRoam);
            }

            return stateGraph;
        }

        private bool MostMembersArrived()
        {
            int pawnsInDestinationRange = 0;
            foreach(Pawn pawn in lord.ownedPawns)
            {
                IntVec3? targetCell = pawn.mindState?.duty?.focus.Cell;
                if(targetCell == null)
                {
                    continue;
                }
                if(pawn.Position.InHorDistOf(targetCell.Value, 12))
                {
                    pawnsInDestinationRange++;
                }
            }
            float arrivedRatio = pawnsInDestinationRange == 0 ? 0 : (float)pawnsInDestinationRange / (float)lord.ownedPawns.Count;
            if(RealisticManhuntersMod.Settings.DevModeEnabled)
                Log.Message($"Pawns arrived: {arrivedRatio.ToStringPercent()}");
            bool passed = arrivedRatio >= RatioOfArrivedPawnsToRoam;
            return passed;
        }

        private bool NoHostileAvailable()
        {
            Pawn pawn = lord.ownedPawns.FirstOrDefault();
            if(pawn == null)
            {
                return false;
            }
            float range = ManhunterUtility.GetPawnFindingRange(pawn);
            foreach(Pawn member in lord.ownedPawns)
            {
                bool isAttacking = member.jobs.curDriver is JobDriver_AttackMelee;
                if(isAttacking)
                {
                    return false;
                }
                bool canAttackNewTarget = TargetUtility.TryFindTarget(member, range) != null;
                if(canAttackNewTarget)
                {
                    return false;
                }
            }
            return true;
        }

        private bool memberDamgedFlag = false;
        private bool MemberDamaged()
        {
            if(!memberDamgedFlag)
            {
                return false;
            }
            memberDamgedFlag = false;
            return true;
        }

        private bool NoDownedPawnsAvailable()
        {
            Pawn pawn = lord.ownedPawns.FirstOrDefault();
            if(pawn == null)
            {
                return false;
            }
            float range = ManhunterUtility.GetPawnFindingRange(pawn);
            return lord.ownedPawns.All(member => TargetUtility.TryFindDownedPawn(member, range) == null);
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_References.Look(ref currentAttackTarget, nameof(currentAttackTarget));
            Scribe_Values.Look(ref currentForcedRoamPosition, nameof(currentForcedRoamPosition), IntVec3.Invalid);
        }
    }
}
