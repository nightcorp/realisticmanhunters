﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace RealisticManhunters
{
    public class LordToil_KillDownedPawns : LordToil
    {
        public override void UpdateAllDuties()
        {
            foreach(Pawn member in lord.ownedPawns)
            {
                member.mindState.duty = new PawnDuty(ReMa_DutyDefOf.ReMa_KillDownedPawns);
                //member.jobs.EndCurrentJob(JobCondition.InterruptForced, false, false);
            }
        }
    }
}
