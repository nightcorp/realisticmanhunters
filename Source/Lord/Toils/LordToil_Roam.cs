﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace RealisticManhunters
{
    public class LordToil_Roam : LordToil
    {
        bool justSpawned = true;
        IntVec3 roamPosition;

        public LordJob_Manhunter Job => lord.LordJob as LordJob_Manhunter;

        public LordToil_Roam() { }

        public override void Init()
        {
            base.Init();
            if(justSpawned)
            {
                if(RealisticManhuntersMod.Settings.DevModeEnabled)
                    Log.Message($"Just spawned, not determining roam position yet");
                return;
            }
            SetRandomRoamPosition();
            if(RealisticManhuntersMod.Settings.DevModeEnabled)
                Log.Message($"Initialized roam toil with position {roamPosition}");
        }

        public override void LordToilTick()
        {
            base.LordToilTick();
            if(justSpawned && lord.ownedPawns.Any())
            {
                justSpawned = false;
                SetRandomRoamPosition();
                UpdateAllDuties();
            }
        }

        public override void UpdateAllDuties()
        {
            PawnDuty duty;
            if(justSpawned)
            {
                duty = new PawnDuty(DutyDefOf.Idle);
            }
            else
            {
                duty = new PawnDuty(ReMa_DutyDefOf.ReMa_Roam, roamPosition, -1f);
            }
            foreach(Pawn pawn in lord.ownedPawns)
            {
                pawn.mindState.duty = duty;
            }
        }

        private void SetRoamPosition(IntVec3 position)
        {
            roamPosition = position;
            if(RealisticManhuntersMod.Settings.DevModeEnabled)
                Map.debugDrawer.FlashLine(lord.ownedPawns.First().Position, roamPosition, 100, SimpleColor.Yellow);
            //foreach(Pawn pawn in lord.ownedPawns)
            //{
            //    pawn.jobs.EndCurrentJob(JobCondition.InterruptForced, canReturnToPool: false);
            //}
        }

        private void SetRandomRoamPosition()
        {
            IntVec3 position = Job.currentForcedRoamPosition;
            if(position == IntVec3.Invalid)
            {
                Pawn pawn = lord.ownedPawns.FirstOrDefault();
                if(pawn == null)
                {
                    return;
                }
                position = PositionUtility.FindRandomRoamPositionFor(pawn);
            }
            else
            {
                if(RealisticManhuntersMod.Settings.DevModeEnabled)
                    Log.Message($"Consumed forced roam position {position}");
                Job.currentForcedRoamPosition = IntVec3.Invalid;
            }
            
            SetRoamPosition(position);
        }

        public void ForceRoamPosition(IntVec3 position)
        {
            SetRoamPosition(position);
            foreach(Pawn pawn in lord.ownedPawns)
            {
                pawn.mindState.duty.focus = position;
                pawn.jobs.EndCurrentJob(JobCondition.InterruptForced, canReturnToPool: false);
            }
        }
    }
}
