﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace RealisticManhunters
{
    public class LordToil_GetFood : LordToil
    {
        Predicate<HungerCategory> predicate;

        public LordToil_GetFood(Predicate<HungerCategory> predicate)
        {
            this.predicate = predicate;
        }

        public override void Init()
        {
            base.Init();
        }

        public override void LordToilTick()
        {
            base.LordToilTick();
        }

        public override void UpdateAllDuties()
        {
            List<Pawn> hungryMembers = lord.ownedPawns.FindAll(p => predicate(p.needs.food.CurCategory));
            if(RealisticManhuntersMod.Settings.DevModeEnabled)
                Log.Message($"members matching hunger predicate: {string.Join(", ", hungryMembers)}");
            if(hungryMembers.NullOrEmpty())
            {
                foreach(Pawn member in lord.ownedPawns)
                {
                    member.mindState.duty = new PawnDuty(DutyDefOf.Idle);
                }
                return;
            }

            foreach(Pawn member in lord.ownedPawns)
            {
                if(hungryMembers.Contains(member))
                {
                    member.mindState.duty = new PawnDuty(ReMa_DutyDefOf.ReMa_GetFood);
                }
                else
                {
                    Pawn escortee = hungryMembers.RandomElement();
                    member.mindState.duty = new PawnDuty(ReMa_DutyDefOf.ReMa_Escort, escortee, 5);
                }
                //member.jobs.EndCurrentJob(JobCondition.InterruptForced, true, false);
            }
        }
    }
}
