﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.SocialPlatforms;
using Verse;
using Verse.AI;
using Verse.AI.Group;
using static UnityEngine.GraphicsBuffer;

namespace RealisticManhunters
{
    public class LordToil_AttackAsPack : LordToil
    {
        LordJob_Manhunter Job => this.lord.LordJob as LordJob_Manhunter;
        Thing Target => Job.currentAttackTarget;
        float EngagementRange => ManhunterUtility.GetPawnFindingRange(lord.ownedPawns.First());

        public override void Init()
        {
            base.Init();
            foreach(Pawn member in lord.ownedPawns)
            {
                member.jobs.EndCurrentJob(JobCondition.InterruptForced, false, false);
            }
        }

        public override void UpdateAllDuties()
        {
            if(Target == null)
            {
                Log.Warning($"No target set in LordJob!");
                return;
            }
            List<Pawn> attackingPawns = new List<Pawn>();
            List<Pawn> reservePawns = new List<Pawn>();

            SetHuntForAllMembersInRangeOfTarget(attackingPawns, reservePawns);

            // if a member was harmed (which caused this toil to start) always force that member to retaliate, regardless of range
            SetRetaliateForAttackedMember(attackingPawns, reservePawns);

            // if no pawns were in range of the target, force some random member to target the enemy and make everyone else escort them
            // a member that was hurt and is scheduled for retaliation may have been one-shot by the attack, so pick a random other member to also attack, this prevents picking off the pack one-by-one with extreme high damage weaponry
            bool needAdditionalAttackers = attackingPawns.NullOrEmpty()
                || (attackingPawns.Count == 1 && attackingPawns[0] == Job.lastAttackedPackMember);
            if (needAdditionalAttackers)
            {
                ForceAttackForRandomPackMembers(attackingPawns, reservePawns);
            }

            foreach(Pawn pawn in reservePawns)
            {
                pawn.mindState.duty = new PawnDuty(ReMa_DutyDefOf.ReMa_Escort, attackingPawns.RandomElement(), 8);
            }
        }

        private void SetHuntForAllMembersInRangeOfTarget(List<Pawn> attackingPawns, List<Pawn> reservePawns)
        {
            foreach (Pawn member in lord.ownedPawns)
            {
                // if in range, attack, otherwise escort
                if (member.Position.DistanceToSquared(Target.Position) <= EngagementRange * EngagementRange)
                {
                    member.mindState.duty = new PawnDuty(ReMa_DutyDefOf.ReMa_HuntTarget, Target, EngagementRange);
                    attackingPawns.Add(member);
                }
                else
                {
                    reservePawns.Add(member);
                }
                //member.jobs.EndCurrentJob(JobCondition.InterruptForced, false, false);
            }
        }

        private void ForceAttackForRandomPackMembers(List<Pawn> attackingPawns, List<Pawn> reservePawns)
        {
            Pawn chosenAttacker = null;
            List<Pawn> potentialPawns = reservePawns
                .Where(p => p != Job.lastAttackedPackMember)
                .ToList();
            if(potentialPawns.Count == 0)
            {
                return;
            }
            chosenAttacker = reservePawns.RandomElement();

            SetForceAttackTargetJob(chosenAttacker);
            attackingPawns.Add(chosenAttacker);
            reservePawns.Remove(chosenAttacker);
            //chosenAttacker.mindState.duty = new PawnDuty(ReMa_DutyDefOf.ReMa_HuntTarget, target, -1);
            if (RealisticManhuntersMod.Settings.DevModeEnabled)
            {
                Log.Message($"No pack member was in range of target, forcing one member to be an attacker: {chosenAttacker}");
            }
        }

        private void SetRetaliateForAttackedMember(List<Pawn> attackingPawns, List<Pawn> reservePawns)
        {
            Pawn lastAttackedPackMember = Job.lastAttackedPackMember;
            if(lastAttackedPackMember == null)
            {
                return;
            }
            if (attackingPawns.Contains(lastAttackedPackMember))
            {
                return;
            }
            SetForceAttackTargetJob(lastAttackedPackMember);
            attackingPawns.Add(lastAttackedPackMember);
            reservePawns.Remove(lastAttackedPackMember);
        }

        private void SetForceAttackTargetJob(Pawn member)
        {
            if (member == null)
            {
                return;
            }
            if(Target == null)
            {
                return;
            }
            Job attackJob = JobMaker.MakeJob(JobDefOf.AttackMelee, Target);
            attackJob.attackDoorIfTargetLost = true;
            member.jobs.TryTakeOrderedJob(attackJob);
        }
    }
}
