﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI.Group;

namespace RealisticManhunters
{
    public class TransitionAction_LogTransition : TransitionAction
    {
        LordToil previousToil;

        public TransitionAction_LogTransition(LordToil previousToil)
        {
            this.previousToil = previousToil;
        }

        public override void DoAction(Transition trans)
        {
            if(RealisticManhuntersMod.Settings.DevModeEnabled)
                Log.Message($"Transitioning toil from {previousToil?.GetType()} to {trans?.target?.GetType()}");
        }
    }
}
