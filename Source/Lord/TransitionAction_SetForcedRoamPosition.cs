﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI.Group;

namespace RealisticManhunters
{
    public class TransitionAction_SetForcedRoamPosition : TransitionAction
    {
        public override void DoAction(Transition trans)
        {
            Lord lord = trans.target.lord;
            if(!(lord.LordJob is LordJob_Manhunter manhunterJob))
            {
                return;
            }
            IEnumerable<IntVec3> memberPositions = lord.ownedPawns.Select(member => member.Position);
            IntVec3 roamRoot = PositionUtility.DetermineCenter(memberPositions);
            manhunterJob.currentForcedRoamPosition = roamRoot;
        }
    }
}
