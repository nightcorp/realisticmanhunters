﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RealisticManhunters
{
    public class RealisticManhuntersMod : Mod
    {
        public static RealisticManhuntersMod Mod;
        public static RealisticManhuntersSettings Settings;

        public RealisticManhuntersMod(ModContentPack content) : base(content)
        {
            Mod = this;
            Settings = GetSettings<RealisticManhuntersSettings>();
        }

        public override string SettingsCategory()
        {
            return "ReMa_SettingsCategory".Translate();
        }

        public override void DoSettingsWindowContents(Rect inRect)
        {
            Listing_Standard list = new Listing_Standard();
            list.Begin(inRect);

            list.CheckboxLabeled("ReMa_ManhuntersKillDownedPawns".Translate(), ref Settings.ManhuntersKillDownedPawns);
            list.CheckboxLabeled("ReMa_ManhuntersAttackAnythingThatMoves".Translate(), ref Settings.ManhuntersAttackAnythingThatMoves, "ReMa_ManhuntersAttackAnythingThatMoves_Tip".Translate());

            Slider(list, "ReMa_ScariaFoodFallRateMultiplier".Translate(Settings.ScariaFoodFallRateMultiplier.ToStringByStyle(ToStringStyle.FloatTwo)), ref Settings.ScariaFoodFallRateMultiplier, 0.1f, 4f, "ReMa_ScariaFoodFallRateMultiplier_Tip".Translate());
            list.Gap();
            if(Prefs.DevMode)
            {
                list.CheckboxLabeled("ReMa_DevMode".Translate(), ref Settings._devModeEnabled, "ReMa_DevMode_Tip".Translate());
            }
            if(Settings.DevModeEnabled)
            {
                Slider(list, "ReMa_TicksToStayNearRoam".Translate(Settings.TicksPerRoam), ref Settings.TicksPerRoam, 1, 100000, "ReMa_TicksToStayNearRoam_Tip".Translate());
                Slider(list, "ReMa_ArrivedRatioForNextRoam".Translate(Settings.RatioOfArrivedPawnsToNextRoam.ToStringPercent()), ref Settings.RatioOfArrivedPawnsToNextRoam, 0.01f, 1f, "ReMa_ArrivedRatioForNextRoam_Tip".Translate());
                Slider(list, "ReMa_ExitMapMultiplier".Translate(Settings.ExitMapTimeMultiplier.ToStringByStyle(ToStringStyle.FloatTwo)), ref Settings.ExitMapTimeMultiplier, 1, 10, "ReMa_ExitMapMultiplier_Tip".Translate());
                Slider(list, "ReMa_DefaultManhuntingRange".Translate(Settings.DefaultManhuntingRange), ref Settings.DefaultManhuntingRange, 10, 200, "ReMa_DefaultManhuntingRange_Tip".Translate());
                Slider(list, "ReMa_DefaultRoamRadius".Translate(Settings.FallbackRoamRadius), ref Settings.FallbackRoamRadius, 10, 200, "ReMa_DefaultRoamRadius_Tip".Translate());
                Slider(list, "ReMa_HungryMembersRatioToEngageFeedingToil".Translate(Settings.HungryMembersRatioToEngageFeedingToil.ToStringPercent()), ref Settings.HungryMembersRatioToEngageFeedingToil, 0.1f, 1, "ReMa_HungryMembersRatioToEngageFeedingToil_Tip".Translate());
                Slider(list, "ReMa_MeleeExpiryMultiplier".Translate(Settings.MeleeExpiryMultiplier.ToStringByStyle(ToStringStyle.FloatTwo)), ref Settings.MeleeExpiryMultiplier, 1f, 10f, "ReMa_MeleeExpiryMultiplier_Tip".Translate());
            }
            list.Gap();
            if(list.ButtonText("ReMa_Reset".Translate()))
            {
                Settings.Reset();
            }
            list.End();
        }

        private static void Slider(Listing_Standard list, string label, ref float value, float min, float max, string tooltip)
        {
#if v1_3
            list.Label(label, tooltip: tooltip);
            value = list.Slider(value, min, max);
#else
            value = list.SliderLabeled(label, value, min, max, 0.3f, tooltip);
#endif
        }
        private static void Slider(Listing_Standard list, string label, ref int value, float min, float max, string tooltip)
        {
#if v1_3
            list.Label(label, tooltip: tooltip);
            value = Mathf.RoundToInt(list.Slider(value, min, max));
#else
            value = Mathf.RoundToInt(list.SliderLabeled(label, value, min, max, 0.3f, tooltip));
#endif
        }
    }


    public class RealisticManhuntersSettings : ModSettings
    {
        public const bool DevModeEnabledDefault = false;
        public bool _devModeEnabled = DevModeEnabledDefault;
        public bool DevModeEnabled => Prefs.DevMode && _devModeEnabled;

        public const bool ManhuntersKillDownedPawnsDefault = true;
        public bool ManhuntersKillDownedPawns = ManhuntersKillDownedPawnsDefault;
        public const bool ManhuntersAttackAnythingThatMovesDefault = false;
        public bool ManhuntersAttackAnythingThatMoves = ManhuntersAttackAnythingThatMovesDefault;

        public const int TicksPerRoamDefault = 10000;
        public int TicksPerRoam = TicksPerRoamDefault;
        public const float RatioOfArrivedPawnsToNextRoamDefault = 0.8f;
        public float RatioOfArrivedPawnsToNextRoam = RatioOfArrivedPawnsToNextRoamDefault;
        public const float ExitMapTimeMultiplierDefault = 4f;
        public float ExitMapTimeMultiplier = ExitMapTimeMultiplierDefault;
        public const float ScariaFoodFallRateMultiplierDefault = 0.5f;
        public float ScariaFoodFallRateMultiplier = ScariaFoodFallRateMultiplierDefault;
        public const float DefaultManhuntingRangeDefault = 50f;
        public float DefaultManhuntingRange = DefaultManhuntingRangeDefault;
        public const int FallbackRoamRadiusDefault = 90;
        public int FallbackRoamRadius = FallbackRoamRadiusDefault;
        public const float HungryMembersRatioToEngageFeedingToilDefault = 0.3f;
        public float HungryMembersRatioToEngageFeedingToil = HungryMembersRatioToEngageFeedingToilDefault;
        public const float MeleeExpiryMultiplierDefault = 3f;
        public float MeleeExpiryMultiplier = MeleeExpiryMultiplierDefault;

        public void Reset()
        {
            ManhuntersKillDownedPawns = ManhuntersKillDownedPawnsDefault;
            ManhuntersAttackAnythingThatMoves = ManhuntersAttackAnythingThatMovesDefault;
            _devModeEnabled = DevModeEnabledDefault;
            TicksPerRoam = TicksPerRoamDefault;
            RatioOfArrivedPawnsToNextRoam = RatioOfArrivedPawnsToNextRoamDefault;
            ExitMapTimeMultiplier = ExitMapTimeMultiplierDefault;
            ScariaFoodFallRateMultiplier = ScariaFoodFallRateMultiplierDefault;
            DefaultManhuntingRange = DefaultManhuntingRangeDefault;
            FallbackRoamRadius = FallbackRoamRadiusDefault;
            HungryMembersRatioToEngageFeedingToil = HungryMembersRatioToEngageFeedingToilDefault;
            MeleeExpiryMultiplier = MeleeExpiryMultiplierDefault;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref ManhuntersKillDownedPawns, nameof(ManhuntersKillDownedPawns), ManhuntersKillDownedPawnsDefault);
            Scribe_Values.Look(ref ManhuntersAttackAnythingThatMoves, nameof(ManhuntersAttackAnythingThatMoves), ManhuntersAttackAnythingThatMovesDefault);
            Scribe_Values.Look(ref _devModeEnabled, nameof(_devModeEnabled), DevModeEnabledDefault);
            Scribe_Values.Look(ref TicksPerRoam, nameof(TicksPerRoam), TicksPerRoamDefault);
            Scribe_Values.Look(ref RatioOfArrivedPawnsToNextRoam, nameof(RatioOfArrivedPawnsToNextRoam), RatioOfArrivedPawnsToNextRoamDefault);
            Scribe_Values.Look(ref ExitMapTimeMultiplier, nameof(ExitMapTimeMultiplier), ExitMapTimeMultiplierDefault);
            Scribe_Values.Look(ref ScariaFoodFallRateMultiplier, nameof(ScariaFoodFallRateMultiplier), ScariaFoodFallRateMultiplierDefault);
            Scribe_Values.Look(ref DefaultManhuntingRange, nameof(DefaultManhuntingRange), DefaultManhuntingRangeDefault);
            Scribe_Values.Look(ref FallbackRoamRadius, nameof(FallbackRoamRadius), FallbackRoamRadiusDefault);
            Scribe_Values.Look(ref HungryMembersRatioToEngageFeedingToil, nameof(HungryMembersRatioToEngageFeedingToil), HungryMembersRatioToEngageFeedingToilDefault);
            Scribe_Values.Look(ref MeleeExpiryMultiplier, nameof(MeleeExpiryMultiplier), MeleeExpiryMultiplierDefault);
        }
    }
}
