﻿//using RimWorld;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Verse;
//using Verse.AI.Group;

//namespace RealisticManhunters
//{
//    public class IncidentWorker_RealisticManhunterPack : IncidentWorker_ManhunterPack
//    {
//        // keep the CanFireNow logic from base manhunter

//        protected override bool TryExecuteWorker(IncidentParms parms)
//        {
//            Map map = (Map)parms.target;
//            PawnKindDef pawnKind = parms.pawnKind;
//            if(pawnKind == null)
//            {
//                ManhunterPackIncidentUtility.TryFindManhunterAnimalKind(parms.points, map.Tile, out pawnKind);
//                if(pawnKind == null)
//                {
//                    return false;
//                }
//            }
//            IntVec3 spawnCenter = parms.spawnCenter;
//            if(!spawnCenter.IsValid && !RCellFinder.TryFindRandomPawnEntryCell(out spawnCenter, map, CellFinder.EdgeRoadChance_Animal, false, null))
//            {
//                return false;
//            }

//            List<Pawn> pawns = ManhunterPackIncidentUtility.GenerateAnimals(pawnKind, map.Tile, parms.points * 1f, parms.pawnCount);
//            Lord lord = LordMaker.MakeNewLord(parms.faction, MakeLordJob(parms), map, pawns);

//            Rot4 rotation = Rot4.FromAngleFlat((map.Center - spawnCenter).AngleFlat);
//            foreach(Pawn pawn in pawns)
//            {
//                IntVec3 spawnPosition = CellFinder.RandomClosewalkCellNear(spawnCenter, map, 10, null);
//                QuestUtility.AddQuestTag(GenSpawn.Spawn(pawn, spawnPosition, map, rotation, WipeMode.Vanish, false), parms.questTag);
//                pawn.health.AddHediff(HediffDefOf.Scaria, null, null, null);
//                pawn.mindState.mentalStateHandler.TryStartMentalState(MentalStateDefOf.ManhunterPermanent, null, false, false, null, false, false, false);
//                pawn.mindState.exitMapAfterTick = Find.TickManager.TicksGame + Rand.Range(60000, 120000);
//            }
//            base.SendStandardLetter("LetterLabelManhunterPackArrived".Translate(), "ManhunterPackArrived".Translate(pawnKind.GetLabelPlural(-1)), LetterDefOf.ThreatBig, parms, new LookTargets(pawns));
//            if(!Prefs.DevMode)
//            {
//                Find.TickManager.slower.SignalForceNormalSpeedShort();
//            }
//            LessonAutoActivator.TeachOpportunity(ConceptDefOf.ForbiddingDoors, OpportunityType.Critical);
//            LessonAutoActivator.TeachOpportunity(ConceptDefOf.AllowedAreas, OpportunityType.Important);
//            return true;
//        }
//    }
//}
