﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using Verse.AI;

namespace RealisticManhunters
{
    public class JobGiver_AIFightEnemies_Manhunter : JobGiver_AIFightEnemies
    {

        protected override Job TryGiveJob(Pawn pawn)
        {
            this.OverrideRanges(pawn);
            Job job;
            try
            {
                job = base.TryGiveJob(pawn);
                if(job != null)
                {
                    job.attackDoorIfTargetLost = true;
                    job.expiryInterval = Mathf.RoundToInt(job.expiryInterval * RealisticManhuntersMod.Settings.MeleeExpiryMultiplier);
                }
            }
            finally
            {
                this.ResetRanges();
            }
            return job;
        }

    }
}
