﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace RealisticManhunters
{
    public class JobGiver_KillDownedPawns : ThinkNode_JobGiver
    {
        protected override Job TryGiveJob(Pawn pawn)
        {
            float range = ManhunterUtility.GetPawnFindingRange(pawn);
            Pawn downedPawn = TargetUtility.TryFindDownedPawn(pawn, range);
            if(downedPawn == null)
            {
                return null;
            }
            Job job = JobMaker.MakeJob(JobDefOf.AttackMelee, downedPawn);
            job.killIncappedTarget = true;
            return job;
        }

    }
}
