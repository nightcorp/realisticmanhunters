﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace RealisticManhunters
{
    public class JobGiver_AIDefendEscortee_Manhunter : JobGiver_AIDefendEscortee
    {
        protected override Job TryGiveJob(Pawn pawn)
        {
            Pawn focusPawn = pawn?.mindState?.duty?.focus.Pawn;
            if(focusPawn == null)
            {
                Log.Error($"No focus pawn for pawn {pawn.LabelShort}, current duty def: {pawn.mindState?.duty?.def} - pack members: {pawn.GetLord()?.ownedPawns?.ToStringSafeEnumerable()}");
                return null;
            }
            this.OverrideRanges(pawn);
            Job job;
            try
            {
                job = base.TryGiveJob(pawn);
            }
            finally
            {
                this.ResetRanges();
            }
            return job;
        }
    }
}
